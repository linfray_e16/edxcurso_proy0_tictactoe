"""
Tic Tac Toe Player
"""

import math
import copy

X = "X"
O = "O"
EMPTY = None


def initial_state():
    """
    Returns starting state of the board.
    """
    return [[EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY]]


def player(board):
    """
    Returns player who has the next turn on a board.
    """
    filled=0
    #This loop checks what cells are filled
    for row in board:
        for cell in row:
            if cell is not EMPTY:
                filled+=1

    #If the number of cell filled is even is X turn, if the number is odd the is O turn
    if (filled%2)==0:
        return X
    else:
        return O




def actions(board):
    """
    Returns set of all possible actions (i, j) available on the board.
    """
    moves=set()
    #If the cell is EMPTY then it can be an action
    for row in range(3):
        for cell in range(3):
            if board[row][cell] is EMPTY:
                moves.add((row,cell))
                
    return moves
 
    

def result(board, action):
    """
    Returns the board that results from making move (i, j) on the board.
    """
    #checking if the move is valid part one
    for m in action:
        if m<0 or m>2:
            raise NameError("Not valid move.")
    
    #checing thata the move is valid part two
    if board[action[0]][action[1]] is not EMPTY:
        raise NameError("Not valid move.That cell it's already occupied")
    
    # Making a hard copy of the board and applying the action
    cboard=copy.deepcopy(board)

    cboard[action[0]][action[1]]=player(board)

    return cboard


def winner(board):
    """
    Returns the winner of the game, if there is one.
    """
    win=None #indicates the winner

    #check the diagonals
    if board[1][1] is not EMPTY:
        if (board[1][1]==board[0][0] and board[1][1]==board[2][2]) or (board[1][1]==board[0][2] and board[1][1]==board[2][0]):
            win=board[1][1]
            #print("gana por diagonal\ wins by diagonal line")
    
    #check rows
    if win is None:
        for row in board:
            if row[0] is not EMPTY and (row[0]==row[1] and row[1]==row[2]):
                win=row[0]
                #print("gana por fila \wins by row line")
                break
    
    #check colums
    if win is None:
        for col in range(3):
            if board[0][col] is not EMPTY and (board[0][col]==board[1][col] and board[1][col]==board[2][col]):
                win=board[0][col]
                #print("gana por columna \ win by column")
                break
    
    return win


def terminal(board):
    """
    Returns True if game is over, False otherwise.
    """
    #checks if there is a winner or if the board if all full
    if winner(board) is not None:
        theend=True
    else:
        for row in board:
            if None in row:
                #print("no hay ganador aun y quedan celdas vacias\ there is no winner and there is still empty cells")
                theend=False
                break
            else:
                theend=True
                #print("todo lleno \ all full")
    
    return theend



def utility(board):
    """
    Returns 1 if X has won the game, -1 if O has won, 0 otherwise.
    """
    if winner(board)==X:
        ut=1
    elif winner(board)==O:
        ut=-1
    else:
        ut=0

    return ut

def max_value(board,alpha,beta):
    """
    Returns the best value that the max player can get from board.
    This also applies alpha beta pruning through the 'alpha' and 'beta' arguments.
    board: it's the current board to be analyzed 
    alpha: the best value that the max player had got until now
    beta: the best value that the min player had got untl now
    """
    #if the board it's terminal, returns the utility of the board
    if terminal(board):
        return utility(board)
    
    #if the board isn't terminal it tries to find the value, looking for the values of all posible
    #moves from the min player in the next move
    v=-math.inf
    for action in actions(board):
        v=max(v,min_value(result(board,action),alpha,beta))
        alpha=max(alpha,v)

        #this condition allows the pruning of a leaf of the actions' tree
        if  beta<= alpha:
            break

    return v    

def min_value(board,alpha,beta):
    """
    Returns the best value that the min player can get from de board.
        This also applies alpha beta pruning through the 'alpha' and 'beta' arguments.
        board: it's the current board to be analyzed 
        alpha: the best value that the max player had got until now
        beta: the best value that the min player had got untl now
    """
    #if the board it's terminal, returns the utility of the board
    if terminal(board):
        return utility(board)
    
    #if the board isn't terminal it tries to find the value, looking for the values of all posible
    #moves from the max player in the next move
    v=math.inf
    for action in actions(board):
        v=min(v,max_value(result(board,action),alpha,beta))
        beta=min(beta,v)

        #this condition allows the pruning of a leaf of the actions' tree
        if beta<=alpha:
            break

    return v 


def minimax(board):
    """
    Returns the optimal action for the current player on the board.
    """
    #if the board is terminal returns None
    if terminal(board):
        best=None
    
    # Depending on which role the AI is playing, it applies minmax process with alpha beta pruning so 
    #the AI select the best action that it can make
    if player(board)==X:  
        mov_val=-math.inf
        alpha=-math.inf
        beta=math.inf
        for action in actions(board):
            val_temp=min_value(result(board,action),alpha,beta)
            if  val_temp> mov_val:
                mov_val=val_temp
                best=action           
    else:
        mov_val=math.inf
        alpha=-math.inf
        beta=math.inf
        for action in actions(board):
            val_temp=max_value(result(board,action),alpha,beta)
            if  val_temp<mov_val:
                mov_val=val_temp
                best=action
                
    return best

